    
# Writing and Reading to files
require 'fileutils'
require 'rails/all'
require "bundler"

RAILS_REQUIREMENT = ">= 7.0"

def assert_minimum_rails_version
  requirement = Gem::Requirement.new(RAILS_REQUIREMENT)
  rails_version = Gem::Version.new(Rails::VERSION::STRING)
  return if requirement.satisfied_by?(rails_version)

  prompt = "This template requires Rails #{RAILS_REQUIREMENT}. "\
           "You are using #{rails_version}. Continue anyway?"
  exit 1 if no?(prompt)
end

def gemfile_requirement(name)
  @original_gemfile ||= IO.read("Gemfile")
  req = @original_gemfile[/gem\s+['"]#{name}['"]\s*(,[><~= \t\d\.\w'"]*)?.*$/, 1]
  req && req.gsub("'", %(")).strip.sub(/^,\s*"/, ', "')
end

def setup_css_js
  copy_file 'app/assets/stylesheets/application.bootstrap.scss', force: true
  copy_file 'app/assets/stylesheets/custom.scss', force: true
  copy_file 'app/javascript/application.js', force: true
  run  'mkdir app/javascript/custom-js'
  copy_file 'app/javascript/custom-js/fixdropdown.js', force: true
  copy_file 'app/javascript/custom-js/hidemenu.js', force: true
  copy_file 'app/javascript/custom-js/resize_nav.js', force: true
end

def create_gemsetfile
  run  'touch ./.ruby-gemset'
end

def source_paths
  if __FILE__ =~ %r{\Ahttps?://}
    require "tmpdir"
    source_paths.unshift(tempdir = Dir.mktmpdir("rt_r7-"))
    at_exit { FileUtils.remove_entry(tempdir) }
    git clone: [
      "--quiet",
      "https://gitlab.com/fivo/rt_r7.git",
      tempdir
    ].map(&:shellescape).join(" ")

    if (branch = __FILE__[%r{rails-template/(.+)/template.rb}, 1])
      Dir.chdir(tempdir) { git checkout: branch }
    end
  else
  [File.expand_path(File.dirname(__FILE__))]
  end
end

def setup_users
  generate 'devise:install'
  environment "config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }",
              env: 'development'
  generate :devise, 'User', 'username:string:uniq', 'admin:boolean'
  rails_command 'db:migrate'
  generate 'devise:views'

  in_root do
    migration = Dir.glob('db/migrate/*').max_by { |f| File.mtime(f) }
    gsub_file migration, /:admin/, ':admin, default: false'
  end
  inject_into_file 'app/controllers/application_controller.rb', before: 'end' do
    "\n  before_action :configure_permitted_parameters, if: :devise_controller?

    protected
    def configure_permitted_parameters
      added_attrs = %i[username email password password_confirmation remember_me]
      devise_parameter_sanitizer.permit :sign_up, keys: added_attrs
      devise_parameter_sanitizer.permit :account_update, keys: added_attrs
    end\n"
  end

  inject_into_file 'app/models/user.rb', before: 'end' do
    "\n validates :username, presence: true, uniqueness: { case_sensitive: false }
        validate :validate_username
        attr_writer :login

        def login
            @login || username || email
        end

        def validate_username
            errors.add(:username, :invalid) if User.where(email: username).exists?
        end

        def self.find_for_database_authentication(warden_conditions)
            conditions = warden_conditions.dup
            if login = conditions.delete(:login)
                where(conditions.to_hash).where(['lower(username) = :value OR lower(email) = :value', {value: login.downcase}]).first
            elsif conditions.key?(:username) || conditions.key?(:email)
                where(conditions.to_h).first
            end
        end\n"
  end

  inject_into_file 'config/initializers/devise.rb', after: '# config.authentication_keys = [:email]' do
    "\n config.authentication_keys = [:login]\n"
  end

  find_and_replace_in_file('app/views/devise/sessions/new.html.erb', 'email', 'login')

  inject_into_file 'app/views/devise/registrations/new.html.erb', before: '<%= f.input :email' do
    "\n<%= f.input :username %>\n"
  end

  inject_into_file 'app/views/devise/registrations/edit.html.erb', before: '<%= f.input :email' do
    "\n<%= f.input :username %>\n"
  end
end

def find_and_replace_in_file(file_name, old_content, new_content)
  text = File.read(file_name)
  new_contents = text.gsub(old_content, new_content)
  File.open(file_name, 'w') { |file| file.write new_contents }
end

def demo_rails_commands
  generate(:controller, 'home index')
  route "root to: 'home#index'"
  rails_command 'g scaffold post title:string body:text --no-scaffold-stylesheet'
  rails_command 'db:migrate'
end

def add_navbar
  navbar = 'app/views/layouts/_navbar.html.erb'
  FileUtils.touch(navbar)
  inject_into_file 'app/views/layouts/application.html.erb', before: '<%= yield %>' do
    "\n<%= render 'layouts/navbar' %>\n"
  end

  append_to_file navbar do
    '<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <%= link_to Rails.application.class.module_parent_name, root_path, class:"navbar-brand" %>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <%= link_to "Home", root_path, class:"nav-link" %>
          </li>
          <li class="nav-item">
            <%= link_to "Posts", posts_path, class:"nav-link" %>
          </li>
        </ul>
        <ul class="navbar-nav ml-auto">
          <% if current_user %>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <%= current_user.username %>
              </a>
              <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                  <%= link_to "Account Settings", edit_user_registration_path, class:"dropdown-item" %>
                  <%= link_to "New Post", new_post_path, class:"dropdown-item" %>
                  <div class="dropdown-divider"></div>
                  <%= link_to "Logout", destroy_user_session_path, method: :delete, class:"dropdown-item" %>
              </div>
          </li>
          <% else %>
          <li class="nav-item">
              <%= link_to "Create Account", new_user_registration_path, class:"nav-link" %>
          </li>
          <li class="nav-item">
            <%= link_to "Login", new_user_session_path, class:"nav-link" %>
          </li>
          <% end %>
        </ul>
      </div>
    </div>
  </nav>'
  end
end

def add_footer
  footer = 'app/views/layouts/_footer.html.erb'
  FileUtils.touch(footer)
  inject_into_file 'app/views/layouts/application.html.erb', after: '<%= yield %>' do
    "\n<%= render 'layouts/footer' %>\n"
  end

  append_to_file footer do
    '<div class="footer-dark">
      <footer>
        <div class="container">
          <div class="row">
            <div class="col-sm-6 col-md-3 item">
              <h3>Services</h3>
              <ul>
                <li><a href="#">Web design</a></li>
                <li><a href="#">Development</a></li>
                <li><a href="#">Hosting</a></li>
              </ul>
            </div>
            <div class="col-sm-6 col-md-3 item">
              <h3>About</h3>
              <ul>
                <li><a href="#">Company</a></li>
                <li><a href="#">Team</a></li>
                <li><a href="#">Careers</a></li>
              </ul>
            </div>
            <div class="col-md-6 item text">
                <h3>Company Name</h3>
                <p>Praesent sed lobortis mi. Suspendisse vel placerat ligula. Vivamus ac sem lacus. Ut vehicula rhoncus elementum. Etiam quis tristique lectus. Aliquam in arcu eget velit pulvinar dictum vel in justo.</p>
            </div>
              <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a><a href="#"><i class="icon ion-social-snapchat"></i></a><a href="#"><i class="icon ion-social-instagram"></i></a></div>
            </div>
          <p class="copyright">Company Name © 2018</p>
        </div>
      </footer>
    </div>'
  end
end

def add_sitemap_rb
  FileUtils.mkdir_p('config')
  sitemaprb = 'config/sitemap.rb'
  FileUtils.touch(sitemaprb)
  append_to_file sitemaprb do
    "\n SitemapGenerator::Interpreter.send(:include, ActionView::Helpers::AssetUrlHelper)
      SitemapGenerator::Interpreter.send(:include, Webpacker::Helper)
      
      SitemapGenerator::Sitemap.default_host = 'https://www.domain.com'
      SitemapGenerator::Sitemap.create include_root: false do
        add root_path, priority: 1.0
      
        add contact_path, priority: 0.8
        add about_path, priority: 0.8
          
        add privacy_path, priority: 0.6
        add imprint_path, priority: 0.5
      end\n"
  end
end

def setup_fontawesome
  run 'yarn add @fortawesome/fontawesome-free'
end

def setup_skylight
  skylight_id = ask("put in skylight id to connect to skylight")
  skylight_id = skylight_id.present? ? skylight_id : "newapp"
  run 'bundle exec skylight setup '#(skylight_id)''
end

def setup_rollbar
  rollbar_id = ask("put in rollbar id to connect to rollbar")
  rollbar_id = rollbar_id.present? ? rollbar_id : "newapp"
  run 'rails generate rollbar '#(rollbar_id)''
end

def setup_simple_form
  generate 'simple_form:install --bootstrap'
end

def stop_spring
  run 'spring stop'
end

def add_dev_db
  inject_into_file 'config/database.yml', before: '# As with config' do
    "\n  
  user: dev
  password: dev\n"
  end
end

def add_test_db
  inject_into_file 'config/database.yml', before: '# The specified database' do
    "\n  
  user: dev
  password: dev\n"
  end
end

def add_alchemyCMS
  run 'rails g alchemy:devise:install'
  run 'rails alchemy:install'
end

def wait_for_db_checked
  if yes? ("db settings checked?")
  end
end

def add_gems
  #gem 'devise'
  gem 'simple_form'
  gem 'alchemy_cms'
  gem 'alchemy-devise'
  #gem "haml-rails", "~> 2.0"
  gem 'sitemap_generator'
  #gem 'rollbar'
  #gem "skylight"
end


#LOGIC
assert_minimum_rails_version
source_paths
add_gems

after_bundle do
  setup_css_js
  create_gemsetfile
  add_navbar
  add_footer
  setup_fontawesome
  add_sitemap_rb
  add_dev_db
  #add_test_db
  wait_for_db_checked
  rails_command "db:create"
  rails_command "db:migrate"
  setup_simple_form
  #setup_rollbar
  #setup_skylight
  demo_rails_commands
  #setup_users
end