# Rails Template by FIVO

## OVERVIEW

Updated Rails Template for new projects working with esbuild.

Compatible with: ruby 3.1.2, Rails 7.0.3.1, node v16.16.0

Gems included: devise, simple_form, ,sitemap_generator, rollbar, skylight

Features added: bootstrap, fontawesome, sitemap, simple navbar and footer

Pre-Conditions:  
VS / RVM / NVM / RAILS / RUBY / PG / NODE / YARN already installed correctly with correct versions

## NEW PROJECT 

Usage to start new Project  
nvm install v16.16.0  
nvm alias default v16.16.0  
rvm install 3.1.2  
rvm use 3.1.2  
rvm gemset create "project"  
rvm gemset use "procekt"  
gem install rails -v 7.0.3  
gem install bundler  

while creating the project the pg database must be created so pg has to be running and accepting incomming connection from localhost
also user / pw must be set to create new databases -> this user and pw musst be worked in app/config/database.yml

rails new <project_name> -m rt_r7/template.rb -d postgresql -j esbuild -c bootstrap

cd "projekt"  
touch .ruby-gemset  
nano .ruby-gemset -> "add projectname as text"  

touch .ruby-version  
nano .ruby-version -> "3.1.2"  

bin/dev  "start local dev server"  
code . "start editor"  

Basic Stuff erstellen:  
rails generate controller home index  
root home#index' -> add into routes.rb 

open localhost:3000 in local browser  

## daily work

bundle  
rails db:migrate (aktualisiert Datenbank änderungen)  
rails s (startet server)  
bin/dev  "start local dev server"
code . "start editor"
open localhost:3000 in local browser
# more setup details

## NVM:

curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash  
source ~/.bashrc  
nvm ls-remote  (check for newer versions)
nvm install v16.16.0  
nvm alias default v16.16.0  

## RVM:

gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB  
\curl -sSL https://get.rvm.io | bash  
source ~/.bashrc  

## YARN/PG:

sudo curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -  
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list  
sudo apt update  
sudo apt install postgresql libpq-dev redis-server redis-tools yarn  
yarn  

## Check Versions:

ruby -v  
rails -v  
pg_config --version  

## SCALINGO CLI: (connect repo with scalingo webserver)

scalingo --region osc-fr1 --app my-app integration-link-create https://gitlab.example.org/my-company/my-app --auto-deploy --branch master

## Add RSA KEY:

Diesen Code in ~/.bashrc einfügen

    #ssh-agent configuration  
    if [ -z "$(pgrep ssh-agent)" ]; then  
        rm -rf /tmp/ssh-*  
        eval $(ssh-agent -s) > /dev/null  
    else  
        export SSH_AGENT_PID=$(pgrep ssh-agent)  
        export SSH_AUTH_SOCK=$(find /tmp/ssh-* -name agent.*)  
    fi  
    if [ "$(ssh-add -l)" == "The agent has no identities." ]; then  
        ssh-add  
    fi  


