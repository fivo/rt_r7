window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 30 || document.documentElement.scrollTop >30 ) {
    document.getElementById("navbar").style.fontSize = "1em";
    document.getElementById("logo").style.width = "8%";
  } else {
    document.getElementById("navbar").style.fontSize = "1.5em";
    document.getElementById("logo").style.width = "10%";
  }
}